package blog

import (
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"strings"
	"time"
)

type Tag struct {
	ID        string     `gorm:"type:varchar(48)" json:"id"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `sql:"index" json:",omitempty"`
	Title     string     `json:"title"`
	Slug      string     `json:"slug"`
	Body      string     `json:"body"`
	Posts     []*Post    `gorm:"many2many:post_tags;"`
}

func (t *Tag) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return err
	}
	return scope.SetColumn("ID", id.String())
}

func (t *Tag) BeforeSave(scope *gorm.Scope) error {
	slug := strings.ToLower(strings.Replace(t.Title, " ", "-", -1))
	t.Slug = slug
	return nil
}
