package blog

type Paginator struct {
	HasMore  bool `json:"hasMore"`
	MorePage int  `json:"morePage"`
	HasLess  bool `json:"hasLess"`
	LessPage int  `json:"lessPage"`
}
