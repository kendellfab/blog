module gitlab.com/kendellfab/blog

go 1.13

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/mvdan/xurls v1.1.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/yuin/goldmark v1.1.18 // indirect
	gitlab.com/kendellfab/fazer v0.10.2
	gitlab.com/kendellfab/web v0.2.2
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	gopkg.in/ini.v1 v1.51.0 // indirect
)
