package blog

import (
	"encoding/json"
	"io"
	"net/url"

	"github.com/jinzhu/gorm"
)

type Manager struct {
	orm *gorm.DB
}

func NewManager(orm *gorm.DB) Manager {
	orm.AutoMigrate(&Post{}, &Tag{})
	return Manager{orm: orm}
}

func (m Manager) GetPost(pid string) (Post, error) {
	var p Post
	err := m.orm.Preload("Tags").Find(&p, Post{ID: pid}).Error
	if err != nil {
		return p, err
	}
	return p, nil
}

func (m Manager) GetPostWithSlug(slug string) (Post, error) {
	var p Post
	unescaped, err := url.QueryUnescape(slug)
	if err != nil {
		return p, err
	}
	err = m.orm.Preload("Tags").Find(&p, Post{Slug: unescaped}).Error
	return p, err
}

func (m Manager) ListPosts(limit, offset int) ([]Post, error) {
	var posts []Post
	// Select("id", "title", "slug", "description").
	err := m.orm.Preload("Tags").Order("created_at desc").Limit(limit).Offset(offset).Find(&posts).Error
	return posts, err
}

func (m Manager) ListActivePosts(limit, offset int) ([]Post, error) {
	var posts []Post
	err := m.orm.Preload("Tags").Order("updated_at desc").Where(&Post{Active: true}).Limit(limit).Offset(offset).Find(&posts).Error
	return posts, err
}

func (m Manager) PageActivePosts(limit, page int) ([]Post, Paginator, error) {
	pg := Paginator{}
	var posts []Post

	var count int
	err := m.orm.Model(&Post{}).Where("active = ?", true).Count(&count).Error

	if err != nil {
		return posts, pg, err
	}

	offset := 0
	if page >= 1 {
		offset = (page - 1) * limit
	}

	err = m.orm.Preload("Tags").Order("updated_at desc").Where(&Post{Active: true}).Limit(limit).Offset(offset).Find(&posts).Error
	if err != nil {
		return posts, pg, err
	}

	if len(posts) == limit && (limit*page) < count {
		pg.HasMore = true
	}
	pg.MorePage = page + 1

	if page > 1 {
		pg.HasLess = true
	}
	pg.LessPage = page - 1

	return posts, pg, nil
}

func (m Manager) ListActiveFeaturedPosts() ([]Post, error) {
	var posts []Post
	err := m.orm.Preload("Tags").Order("updated_at desc").Where(&Post{Active: true, Featured: true}).Find(&posts).Error
	return posts, err
}

func (m Manager) ListSlugUpdatedActive() ([]Post, error) {
	var posts []Post
	err := m.orm.Select([]string{"slug", "updated_at"}).Order("updated_at desc").Where(&Post{Active: true}).Find(&posts).Error
	return posts, err
}

func (m Manager) NewPost(r io.Reader) (Post, error) {
	var p Post
	err := json.NewDecoder(r).Decode(&p)
	if err != nil {
		return p, err
	}
	m.orm.Save(&p)
	return p, nil
}

func (m Manager) NewTag(r io.Reader) (Tag, error) {
	var t Tag
	err := json.NewDecoder(r).Decode(&t)
	if err != nil {
		return t, err
	}
	m.orm.Save(&t)
	return t, nil
}

func (m Manager) UpdatePost(r io.Reader) error {
	var p Post
	err := json.NewDecoder(r).Decode(&p)
	if err != nil {
		return err
	}
	return m.orm.Model(&p).Updates(p).Error
}

func (m Manager) ToggleActive(pid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Update("active", !p.Active).Error
}

func (m Manager) ToggleFeatured(pid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Update("featured", !p.Featured).Error
}

func (m Manager) ListTags() ([]Tag, error) {
	var tags []Tag
	err := m.orm.Find(&tags).Error
	return tags, err
}

func (m Manager) GetTagWithPosts(tid string, limit, offset int) (Tag, error) {
	var t Tag
	err := m.orm.Find(&t, Tag{ID: tid}).Error
	if err != nil {
		return t, err
	}

	var posts []*Post
	err = m.orm.Model(&t).Limit(limit).Offset(offset).Related(&posts, "Posts").Error
	if err != nil {
		return t, err
	}
	t.Posts = posts
	return t, nil
}

func (m Manager) AddTagToPost(pid, tid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	var t Tag
	err = m.orm.Find(&t, Tag{ID: tid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Association("Tags").Append(t).Error
}

func (m Manager) RemoveTagFromPost(pid, tid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Association("Tags").Delete(&Tag{ID: tid}).Error
}
