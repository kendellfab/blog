{{define "Content"}}
    <div class="section" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="columns">
            <div class="column" id="tags">
                <div class="box">
                    <div class="level is-mobile">
                        <div class="level-left">
                            <div class="level-item">
                                <h2 class="is-size-4">Tags</h2>
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="level-item">
                                <button class="button is-primary" v-on:click="displayModal();">Add Tag</button>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <table class="table is-striped is-fullwidth">
                        <thead>
                        <tr>
                            <th>Title</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="t in tags">
                            <td>[[t.title]]</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal" v-bind:class="{'is-active': showModal}">
                    <div class="modal-background"></div>
                    <div class="modal-card">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Add Tag</p>
                            <button class="delete" aria-label="close" v-on:click="cancelModal();"></button>
                        </header>
                        <section class="modal-card-body">
                            <div class="field">
                                <label class="label">Title</label>
                                <div class="control">
                                    <input class="input" type="text" v-model="newTag.title" placeholder="Tag Title" />
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Description</label>
                                <div class="control">
                                    <textarea class="textarea" v-model="newTag.body"></textarea>
                                </div>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button class="button is-success" v-on:click="saveTag();">Save Tag</button>
                            <button class="button" v-on:click="cancelModal();">Cancel</button>
                        </footer>
                    </div>
                </div>
            </div>
            <div class="column is-three-quarters" id="posts">
                <div class="box">
                    <div class="level is-mobile">
                        <div class="level-left">
                            <div class="level-item">
                                <h2 class="is-size-4">Posts</h2>
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="level-item">
                                <button class="button is-primary" v-on:click="displayModal();">Add Post</button>
                            </div>
                        </div>
                    </div>
                    <hr />

                    <table class="table is-fullwidth is-striped">
                        <thead>
                        <tr>
                            <th class="is-hidden-mobile">Cover</th>
                            <th>Title</th>
                            <th class="is-hidden-mobile">Created</th>
                            <th class="is-hidden-mobile">Tags</th>
                            <th>Active</th>
                            <th>Featured</th>
                            <th class="is-hidden-mobile">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="p in posts">
                            <td class="is-hidden-mobile">
                                <img :src="p.cover" width="100" />
                            </td>
                            <td><a :href="'/admin/edit/'+p.id">[[p.title]]</a></td>
                            <td class="is-hidden-mobile">[[formatTime(p.createdAt)]]</td>
                            <td class="is-hidden-mobile">
                                <ul class="taglist">
                                    <li v-for="t in p.tags">
                                        [[t.title]]
                                    </li>
                                </ul>
                            </td>
                            <td><label class="checkbox"><input type="checkbox" v-model="p.active" @change="toggleActive(p);"></label></td>
                            <td><label class="checkbox"><input type="checkbox" v-model="p.featured" @change="toggleFeatured(p);"></label></td>
                            <td class="is-hidden-mobile">Delete</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr />
                    <section class="section">
                        <nav class="pagination is-centered">
                            <a class="pagination-previous" v-bind:class="{disabled: hideNew}" @click="loadNewer()" :disabled="hideNew">Newer</a>
                            <a class="pagination-next" v-bind:class="{disabled: hideOld}" @click="loadOlder()" :disabled="hideOld">Older</a>
                        </nav>
                    </section>
                </div>

                <div class="modal" v-bind:class="{'is-active': showModal}">
                    <div class="modal-background"></div>
                    <div class="modal-card">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Add Post</p>
                            <button class="delete" aria-label="close" v-on:click="cancelModal();"></button>
                        </header>
                        <section class="modal-card-body">
                            <div class="field">
                                <label class="label">Title</label>
                                <div class="control">
                                    <input class="input" type="text" v-model="newPost.title" placeholder="New Post" />
                                </div>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button class="button is-success" v-on:click="savePost();">Save Post</button>
                            <button class="button" v-on:click="cancelModal();">Cancel</button>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{end}}

{{define "Js"}}
<script>
    new Vue({
        el: "#tags",
        data: {
            tags: [],
            showModal: false,
            newTag: {}
        },
        methods: {
            loadTags: function() {
                var self = this;
                axios.get("/api/blog/tag").then(function(response) {
                    self.tags = response.data;
                }).catch(function(error) {
                    alert(JSON.stringify(error));
                });
            },
            displayModal: function() {
                this.showModal = true;
            },
            cancelModal: function() {
                this.newTag = {};
                this.showModal = false;
            },
            saveTag: function() {
                var self = this;
                axios.post("/api/blog/tag", self.newTag).then(function(response) {
                    self.loadTags();
                    self.cancelModal();
                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            }
        },
        mounted: function() {
            this.loadTags();
        }
    });

    new Vue({
        el: "#posts",
        data: {
            posts: [],
            limit: 15,
            page: 1,
            showModal: false,
            newPost: {},
            hideNew: false,
            hideOld: true
        },
        methods: {
            loadPosts: function() {
                var self = this;
                axios.get("/api/blog/post?limit="+self.limit+"&page="+self.page).then(function(response) {
                    self.posts = response.data;

                    self.hideNew = self.page === 1;

                    self.hideOld = self.posts.length !== self.limit;
                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            },
            displayModal: function() {
                this.showModal = true;
            },
            cancelModal: function() {
                this.newPost = {};
                this.showModal = false;
            },
            savePost: function() {
                var self = this;
                // Set a user id on the post.
                // self.newPost.authorId = user.ID.toString();
                axios.post("/api/blog/post", self.newPost).then(function(response) {
                    self.loadPosts();
                    self.cancelModal();
                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            },
            toggleActive: function (p) {
                var self = this;
                axios.post("/api/blog/post/"+p.id+ "/active").then(function(response) {

                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            },
            toggleFeatured: function(p) {
                var self = this;
                axios.post("/api/blog/post/"+p.id+ "/featured").then(function(response) {

                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            },
            loadNewer: function () {
                this.page -= 1;
                this.loadPosts();
            },
            loadOlder: function () {
                this.page += 1;
                this.loadPosts();
            }
        },
        mounted: function() {
            this.loadPosts();
        }
    });
</script>
{{end}}