package main

import (
	"flag"
	"log"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/kendellfab/blog"
	"gitlab.com/kendellfab/fazer"
	"gitlab.com/kendellfab/web/upload"
)

func main() {

	var manifestPath string
	flag.StringVar(&manifestPath, "m", filepath.Join("cmd", "example", "static", "manifest.json"), "Set the path to the fazer manifest file.")
	var assetPath string
	flag.StringVar(&assetPath, "a", filepath.Join("cmd", "example", "static", "assets"), "Set the path to the static frontend assets.")
	var tplDir string
	flag.StringVar(&tplDir, "t", filepath.Join("cmd", "example", "static", "tpls"), "Set the path to the templates directory.")
	var uploadDir string
	flag.StringVar(&uploadDir, "u", filepath.Join("cmd", "example", "uploads", "blog"), "Set the uploads directory")
	flag.Parse()

	db, err := gorm.Open("sqlite3", "blog.db")
	if err != nil {
		log.Fatal("Could not open database")
	}
	defer db.Close()

	mux := chi.NewRouter()
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)

	manifest := fazer.MustDecodeManifestJson(manifestPath, fazer.AssetsPath(assetPath))
	fzr := fazer.NewFazer(nil, tplDir, nil, false)
	fzr.AddManifest(manifest)
	fzr.RegisterTemplate("post_meta", "partials/post_meta.gohtml")

	manager := blog.NewManager(db)

	frontendHandler := FrontendHandler{Fazer: fzr, Manager: manager}
	adminHandler := AdminHandler{Fazer: fzr, Manager: manager}
	mux.Get("/", frontendHandler.GetIndex)
	mux.Get("/blog/{slug}", frontendHandler.GetPost)
	mux.Get("/admin", adminHandler.GetList)
	mux.Get("/admin/edit/{id}", adminHandler.GetEdit)

	// region BlogApi
	blogMux := chi.NewRouter()
	blogMux.Use(func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			// Check a session/token/something else here

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	})
	handler := blog.NewHandler(manager, func(r *http.Request, name string) string {
		return chi.URLParam(r, name)
	})
	handler.RegisterRoutes(blogMux)
	mux.Mount("/api/blog", blogMux)
	// endregion

	blogUploadHandler := upload.NewImageUpload("/uploads/blog", upload.NewFileStorage(uploadDir))
	mux.Post("/uploads/blog", blogUploadHandler.DoUpload)
	mux.Get("/uploads/blog/{year}/{month}/{file}", blogUploadHandler.HandleGet)

	mux.Handle("/*", http.FileServer(http.Dir(assetPath)))

	http.ListenAndServe(":3000", mux)
}

func queryInt(r *http.Request, key string, def int) int {
	val := def
	str := r.URL.Query().Get(key)
	if str != "" {
		if i, iErr := strconv.Atoi(str); iErr == nil {
			val = i
		}
	}
	return val
}
