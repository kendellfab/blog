package blog

import (
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type Post struct {
	ID          string     `gorm:"type:varchar(48)" json:"id"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `sql:"index" json:",omitempty"`
	Title       string     `json:"title" gorm:"type:varchar(2048)"`
	Slug        string     `json:"slug" gorm:"index:idx_slug;type:varchar(2048)"`
	Body        string     `json:"body" gorm:"type:text"`
	Description string     `json:"description" gorm:"type:text"`
	Featured    bool       `json:"featured"`
	Active      bool       `json:"active"`
	AuthorID    string     `json:"authorId" gorm:"type:varchar(1024)"`
	Tags        []*Tag     `gorm:"many2many:post_tags;" json:"tags"`
	Cover       string     `json:"cover" gorm:"type:varchar(2048)"`
	CoverAlt    string     `json:"coverAlt" gorm:"type:text"`

	OgURL              string `json:"ogurl"`
	OgTitle            string `json:"ogtitle"`
	OgDescription      string `json:"ogdescription"`
	OgImage            string `json:"ogimage"`
	OgImageType        string `json:"ogimagetype"`
	OgImageWidth       string `json:"ogimagewidth"`
	OgImageHeight      string `json:"ogimageheight"`
	FbAppID            string `json:"fbappid"`
	OgType             string `json:"ogtype"`
	OgLocale           string `json:"oglocale"`
	TwitterCard        string `json:"twittercard"`
	TwitterSite        string `json:"twittersite"`
	TwitterTitle       string `json:"twittertitle"`
	TwitterDescription string `json:"twitterdescription"`
	TwitterCreator     string `json:"twittercreator"`
	TwitterImage       string `json:"twitterimage"`
}

// GetTitle returns the page title, intended to be called from the template with a default value
func (p Post) GetTitle(def string) string {
	if p.Title == "" {
		return def
	}
	return p.Title
}

// GetDescription returns page description, intended to be called from the template with a default value
func (p Post) GetDescription(def string) string {
	if p.Description == "" {
		return def
	}
	return p.Description
}

// GetOgUrl returns the open graph url or the canonical url
func (p Post) GetOgUrl(def string) string {
	if p.OgURL != "" {
		return p.OgURL
	}
	return def
}

// GetOgTitle returns the open graph title for the page, or the regular title, or the default value passed in
func (p Post) GetOgTitle(def string) string {
	if p.OgTitle != "" {
		return p.OgTitle
	} else if p.Title != "" {
		return p.Title
	} else {
		return def
	}
}

// GetOgDescription returns the open graph description, the regular description, or the default value passed in
func (p Post) GetOgDescription(def string) string {
	if p.OgDescription != "" {
		return p.OgDescription
	} else if p.Description != "" {
		return p.Description
	} else {
		return def
	}
}

// GetOgImage returns the open graph image or the default value passed in
func (p Post) GetOgImage(def string) string {
	if p.OgImage != "" {
		return p.OgImage
	}
	return def
}

// GetFbAppId returns the fb app id or the default value passed in
func (p Post) GetFbAppId(def string) string {
	if p.FbAppID != "" {
		return p.FbAppID
	}
	return def
}

// GetOgType returns the open graph type or the default website
func (p Post) GetOgType() string {
	if p.OgType != "" {
		return p.OgType
	}
	return "website"
}

// GetOgLocale returns the open graph locale or the default locale
func (p Post) GetOgLocale() string {
	if p.OgLocale != "" {
		return p.OgLocale
	}
	return "en_US"
}

// GetTwitterCard returns the twitter card summary
func (p Post) GetTwitterCard(def string) string {
	if p.TwitterCard != "" {
		return p.TwitterCard
	} else if p.Title != "" {
		return p.Title
	}
	return def
}

// GetTwitterSite returns the twitter site or default.  i.e. @KendellFab
func (p Post) GetTwitterSite(def string) string {
	if p.TwitterSite != "" {
		return p.TwitterSite
	}
	return def
}

// GetTwitterTitle returns the twitter title or the page title or the default
func (p Post) GetTwitterTitle(def string) string {
	if p.TwitterTitle != "" {
		return p.TwitterTitle
	} else if p.Title != "" {
		return p.Title
	}
	return def
}

// GetTwitterDescription returns the twitter description or the page description or the default
func (p Post) GetTwitterDescription(def string) string {
	if p.TwitterDescription != "" {
		return p.TwitterDescription
	} else if p.Description != "" {
		return p.Description
	}
	return def
}

// GetTwitterCreator returns the twitter creator or default
func (p Post) GetTwitterCreator(def string) string {
	if p.TwitterCreator != "" {
		return p.TwitterCreator
	}
	return def
}

// GetTwitterImage returns the twitter image or default
func (p Post) GetTwitterImage(def string) string {
	if p.TwitterImage != "" {
		return p.TwitterImage
	}
	return def
}

func (p *Post) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return err
	}
	return scope.SetColumn("ID", id.String())
}

func (p *Post) BeforeSave(scope *gorm.Scope) error {
	slug := strings.ToLower(strings.Replace(p.Title, " ", "-", -1))
	p.Slug = slug
	return nil
}

func (p *Post) BeforeUpdate(scope *gorm.Scope) error {
	slug := strings.ToLower(strings.Replace(p.Title, " ", "-", -1))
	return scope.SetColumn("slug", slug)
}
