package blog

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"log"
	"net/http"
	"strconv"
)

type ParamExtractor func(r *http.Request, name string) string

type Handler struct {
	manager Manager
	pe      ParamExtractor
}

func NewHandler(m Manager, pe ParamExtractor) Handler {
	return Handler{manager: m, pe: pe}
}

// RegisterRoutes takes a mux, should be a sub router with the desired auth middleware attached
// All of these routes are intended to be secure as they are for authoring of the posts.
func (h Handler) RegisterRoutes(mux *chi.Mux) {
	mux.Get("/post/{pid}", h.GetPost)
	mux.Post("/post", h.AddPost)
	mux.Get("/post", h.ListPosts)
	mux.Post("/post/{pid}", h.UpdatePost)
	mux.Post("/post/{pid}/active", h.ToggleActive)
	mux.Post("/post/{pid}/featured", h.ToggleFeatured)
	mux.Get("/tag", h.ListTags)
	mux.Post("/tag", h.AddTag)
	mux.Put("/post/{pid}/tag/{tid}", h.AddTagToPost)
	mux.Delete("/post/{pid}/tag/{tid}", h.RemoveTagFromPost)
	mux.Get("/tag/{tid}/posts", h.GetTagWithPosts)
}

func (h Handler) GetPost(w http.ResponseWriter, r *http.Request) {
	post, err := h.manager.GetPost(h.pe(r, "pid"))
	if err != nil {
		log.Println("error loading post", err)
		http.Error(w, "error loading post", http.StatusInternalServerError)
		return
	}
	h.renderJson(w, r, post)
}

func (h Handler) ListPosts(w http.ResponseWriter, r *http.Request) {
	limit := h.queryInt(r, "limit", 15)
	page := h.queryInt(r, "page", 1)
	posts, err := h.manager.ListPosts(limit, (page-1)*limit)
	if err != nil {
		log.Println("error loading posts: limit ->", limit, " page ->", page)
		http.Error(w, "error loading posts", http.StatusInternalServerError)
		return
	}
	h.renderJson(w, r, posts)
}

func (h Handler) AddPost(w http.ResponseWriter, r *http.Request) {
	post, err := h.manager.NewPost(r.Body)
	if err != nil {
		log.Println("error creating post", err)
		http.Error(w, "error creating post", http.StatusInternalServerError)
		return
	}
	h.renderJson(w, r, post)
}

func (h Handler) UpdatePost(w http.ResponseWriter, r *http.Request) {
	err := h.manager.UpdatePost(r.Body)
	if err != nil {
		log.Println("error updating post", err)
		http.Error(w, "error updating post", http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "post updated")
}

func (h Handler) ToggleActive(w http.ResponseWriter, r *http.Request) {
	pid := h.pe(r, "pid")
	err := h.manager.ToggleActive(pid)
	if err != nil {
		log.Println("error toggling active", err)
		http.Error(w, "error toggling active", http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "post updated")
}

func (h Handler) ToggleFeatured(w http.ResponseWriter, r *http.Request) {
	pid := h.pe(r, "pid")
	err := h.manager.ToggleFeatured(pid)
	if err != nil {
		log.Println("error toggling featured", err)
		http.Error(w, "error toggling featured", http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "post updated")
}

func (h Handler) AddTag(w http.ResponseWriter, r *http.Request) {
	tag, err := h.manager.NewTag(r.Body)
	if err != nil {
		log.Println("error creating tag", err)
		http.Error(w, "error creating tag", http.StatusInternalServerError)
		return
	}
	h.renderJson(w, r, tag)
}

func (h Handler) ListTags(w http.ResponseWriter, r *http.Request) {
	tags, err := h.manager.ListTags()
	if err != nil {
		log.Println("error loading tags", err)
		http.Error(w, "error loading tags", http.StatusInternalServerError)
		return
	}
	h.renderJson(w, r, tags)
}

func (h Handler) AddTagToPost(w http.ResponseWriter, r *http.Request) {
	pid := h.pe(r, "pid")
	tid := h.pe(r, "tid")
	err := h.manager.AddTagToPost(pid, tid)
	if err != nil {
		log.Println("error associating tag with post", err)
		http.Error(w, "error associating tag with post", http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "associated tag with post")
}

func (h Handler) RemoveTagFromPost(w http.ResponseWriter, r *http.Request) {
	pid := h.pe(r, "pid")
	tid := h.pe(r, "tid")
	err := h.manager.RemoveTagFromPost(pid, tid)
	if err != nil {
		log.Println("error removing tag from post", err)
		http.Error(w, "error removing tag from post", http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "removed tag from post")
}

func (h Handler) GetTagWithPosts(w http.ResponseWriter, r *http.Request) {
	tid := h.pe(r, "tid")
	limit := h.queryInt(r, "limit", 15)
	page := h.queryInt(r, "page", 1)
	tag, err := h.manager.GetTagWithPosts(tid, limit, (page-1)*limit)
	if err != nil {
		log.Println("error load tag with posts", err)
		http.Error(w, "error loading tag with posts", http.StatusInternalServerError)
		return
	}
	h.renderJson(w, r, tag)
}

func (h Handler) queryInt(r *http.Request, key string, def int) int {
	val := def
	str := r.URL.Query().Get(key)
	if str != "" {
		if i, iErr := strconv.Atoi(str); iErr == nil {
			val = i
		}
	}
	return val
}

func (h Handler) renderJson(w http.ResponseWriter, r *http.Request, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
